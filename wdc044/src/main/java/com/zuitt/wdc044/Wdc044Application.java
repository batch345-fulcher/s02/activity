package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);

	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "User") String name){
		return String.format("Hi %s", name);
	}

	@GetMapping("/nameAge")
	public String nameAge(
		@RequestParam(value = "name", defaultValue = "User") String name,
		@RequestParam(value = "age", defaultValue = "Unknown") String age){
			return String.format("Hello %s!", name) + String.format(" Your age is %s", age);
	}



}
