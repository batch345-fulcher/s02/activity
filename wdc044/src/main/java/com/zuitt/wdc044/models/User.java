package com.zuitt.wdc044.models;

import javax.persistence.*;
@Entity
@Table(name = "users")
public class User {
    // Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;
    @Column
    private String password;

    // Constructors
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters and Setters
    public String getUsername() {
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
}
