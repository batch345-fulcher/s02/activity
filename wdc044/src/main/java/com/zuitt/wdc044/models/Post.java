package com.zuitt.wdc044.models;

import javax.persistence.*;

// @Entity - Marks this Java object as a representation of a database table via @Entity annotation
@Entity

@Table(name = "posts")
public class Post {
    // @Id - Indicate that this property represents the primary key;
    // @GeneratedValue - Values for this property will be auto-incremented
    @Id
    @GeneratedValue
    private Long id;

    // @Column - Class properties that represent table columns in a relational database and is annotated by Column
    @Column
    private String title;

    @Column
    private String content;

    // Constructors
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // Getters and Setters
    public String getTitle(){
        return this.title;
    }
    public String getContent() {
        return this.content;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
